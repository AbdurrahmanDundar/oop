﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Student: Person
    {
		public static List<Student> List { get; set; }
		public Student(string firstName, string lastName, DateTime birthday, int id, int schoolId, string contactNumber) : base(firstName, lastName, birthday, id, schoolId, contactNumber)
		{
		}
		public static string ShowAll()
		{
			string text = "Lijst van studenten:\n";
			foreach (Student student in List)
			{
				text += $"{student.FirstName}, {student.LastName}, {student.Birthday}, {student.Id},{student.SchoolId}\n";
			}
			return text;
		}

		public override string ShowOne()
		{
			return $"Gegevens van de student: {this.FirstName}, {this.LastName}, {this.Birthday}, {this.Id}, {this.SchoolId} ";
		}

		public override string GetNameTagText()
		{
			return $"{this.FirstName} {this.LastName}";
		}
	}
}
