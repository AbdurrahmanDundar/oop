﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    abstract class Person
    {
		private String firstName;
		private String lastName;
		protected DateTime Birthday { get; set; }
		public int Id { get; set; }
		public int SchoolId { get; set; }
		protected string ContactNumber{ get; set; }



		public string FirstName
		{
			get { return firstName; }
			set
			{
				if (string.IsNullOrEmpty(firstName))
				{
					firstName = value;
				}
			}
		}
		public string LastName
		{
			get { return lastName; }
			set
			{
				if (string.IsNullOrEmpty(lastName))
				{
					lastName = value;
				}
			}
		}

		public Person(string firstName, string lastName, DateTime birthday, int id, int schoolId, string contactNumber)
		{
			this.Birthday = birthday;
			this.Id = id;
			this.SchoolId = schoolId;
			this.FirstName = firstName;
			this.LastName = lastName;
			this.ContactNumber = contactNumber;
		}

		public Person()
		{
			//default
		}
		public abstract string ShowOne();
        public virtual string GetNameTagText() 
        {
            return $"{this.FirstName} {this.LastName}";
        }
    }
}
