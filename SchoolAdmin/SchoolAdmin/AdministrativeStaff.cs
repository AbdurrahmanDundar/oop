﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class AdministrativeStaff: Person
    {
		public static List<AdministrativeStaff> List { get; set; }
		public AdministrativeStaff(string firstName, string lastName, DateTime birthday, int id, int schoolId, string contactNumber) : base(firstName, lastName, birthday, id, schoolId, contactNumber)
		{
		}

		public static string ShowAll()
		{
			string text = "Lijst van administratieve werknemers:\n";
			foreach (AdministrativeStaff am in List)
			{
				text += $"{am.FirstName}, {am.LastName}, {am.Birthday}, {am.Id},{am.SchoolId}\n";
			}
			return text;
		}

		public override string ShowOne()
		{
			return $"Gegevens van de administratieve werknemer: {this.FirstName}, {this.LastName}, {this.Id}, {this.SchoolId} ";
		}

		public override string GetNameTagText()
		{
			return $"(ADMINISTRATIE) {this.FirstName} {this.LastName} (Contact Number ) {this.ContactNumber}";
		}
	}
}
