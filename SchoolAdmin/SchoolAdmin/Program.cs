﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Schoolbeheer");
            School school1 = new School("Go! BS de Spits", "Thonetlaan 106", "2050", 1, "Antwerpen");

            School school2 = new School("Go! Koninklijk Atheneum Deurne", "Fr. Craeybeckxlaan 22", "2100", 2, "Deurne");

            School.List = new List<School>();

            School.List.Add(school1);
            School.List.Add(school2);
            Console.WriteLine(School.ShowAll());

            Student Mo = new Student("Mohamed", "El Farisi",new DateTime(1987,12,06), 1, 1, "1");
            Student Sa = new Student("Sarah", "Jansens", new DateTime(1991,10,21), 2, 1, "2");
            Student Ba = new Student("Bart", "Jansens", new DateTime(1990,10,21), 2, 3, "3");
            Student Fa = new Student("Farah", "El Farisi", new DateTime(1987,12,06), 1, 4, "4");

            Student.List = new List<Student>();

            Student.List.Add(Mo);
            Student.List.Add(Sa);
            Student.List.Add(Ba);
            Student.List.Add(Fa);
            Console.WriteLine(Student.ShowAll());

            Lecturer Adem = new Lecturer("Adem", "Kaya", new DateTime(1976, 12, 01), 1, 1, "5");
            Lecturer Anne = new Lecturer("Anne", "Wouters", new DateTime(1968, 04, 03), 2, 2, "6");
            AdministrativeStaff Rauuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuul = new AdministrativeStaff("Raul", "Jacob", new DateTime(1985, 11, 01), 1, 1, "7");


            Lecturer.List = new List<Lecturer>();

            Lecturer.List.Add(Adem);
            Lecturer.List.Add(Anne);
            Console.WriteLine(Lecturer.ShowAll());

            AdministrativeStaff.List = new List<AdministrativeStaff>();
            AdministrativeStaff.List.Add(Rauuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuul);
            Console.WriteLine(AdministrativeStaff.ShowAll());

            Console.WriteLine(Adem.ShowOne());
            Console.WriteLine(Adem.ShowTaughtCourses());
            Console.WriteLine(Fa.ShowOne());
            Console.WriteLine(Rauuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuul.ShowOne());
            Console.WriteLine("\n");

            var personList = new List<Person>();
            personList.Add(Mo);
            personList.Add(Sa);
            personList.Add(Ba);
            personList.Add(Fa);
            personList.Add(Adem);
            personList.Add(Anne);
            personList.Add(Rauuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuul);
            Console.WriteLine("Alle name tags van de personen.");
            foreach (Person person in personList)
            {
                Console.WriteLine(person.GetNameTagText());
            }

            TheoryCourse theoryCourse = new TheoryCourse("OO Programmeren", 3);
            Seminar seminar = new Seminar("Docker");
            LabCourse lb = new LabCourse("Electricity", 2, "Kniptang");
            Adem.Courses.Add(theoryCourse);
            Adem.Courses.Add(seminar);
            Adem.Courses.Add(lb);
            Console.WriteLine($"Cursussen van Adem: {Adem.ShowTaughtCourses()}");
            Console.ReadLine();

        }
    }
}
