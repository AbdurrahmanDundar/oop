﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Lecturer: Person
    {
		public List<Course> Courses { get; set; }
		public static List<Lecturer> List { get; set; }
		public Lecturer(string firstName, string lastName, DateTime birthday, int id, int schoolId, string contactNumber) : base(firstName, lastName, birthday, id, schoolId, contactNumber)
		{
			this.Courses = new List<Course>();
		}

		public static string ShowAll()
		{
			string text = "Lijst van lecturers:\n";
			foreach (Lecturer lecturer in List)
			{
				text += $"{lecturer.FirstName}, {lecturer.LastName}, {lecturer.Birthday}, {lecturer.Id},{lecturer.SchoolId}\n";
			}
			return text;
		}

		public override string ShowOne()
		{
			return $"Gegevens van de lecturer: {this.FirstName}, {this.LastName}, {this.Birthday}, {this.Id}, {this.SchoolId} ";
		}

		public string ShowTaughtCourses()
		{
			string result = "Vakken van deze lector: ";
			foreach (var course in this.Courses)
			{
				result += $"{course.Title} ({course.CalculateWorkload()})\n";
			}
			return result;
		}

		public override string GetNameTagText()
		{
			return $"(LECTOR) {this.FirstName} {this.LastName} (Contact Number ) {this.ContactNumber}";
		}
	}
}
