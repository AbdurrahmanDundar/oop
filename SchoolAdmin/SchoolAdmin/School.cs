﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class School
    {
		private string name;
		public string Street{ get; set; }
		public string PostalCode{ get; set; }
		public string City{ get; set; }
		public int Id{ get; set; }
		public static List<School> List { get; set; }

		public string Name
		{
			get { return name; }
			set 
			{
				if (string.IsNullOrEmpty(name))
				{
					name = value;
				}
			}
		}


		public School(string street, string postalCode, string city, int id, string name)
		{
			this.Street = street;
			this.PostalCode = postalCode;
			this.City = city;
			this.Id = id;
			this.Name = name;
		}
		public School()
		{
			//default
		}


		public static string ShowAll()
		{
			string text = "Lijst van scholen:\n";
			foreach(School school in List)
			{
				text += $"{school.name}, {school.Street}, {school.City}, {school.Id}\n";
			}
			return text;
		}

		public string ShowOne()
		{
			return $"Gegevens van de school: {this.Name}, {this.Street}, {this.City}, {this.Id} ";
		}

	}
}
