﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class PrijzenMetForeach
    {
        public static void Main()
        {
            static void AskForPrices()
            {
                double[] prijzen = new double[5];
                double gemiddelde = 0;
                Console.WriteLine("Gelieve 20 prijzen in te geven");
                int counter = 0;
                while (counter < 5)
                {
                    prijzen[counter] = Convert.ToDouble(Console.ReadLine());
                    counter++;
                }

                Console.WriteLine("\n");
                foreach (double i in prijzen)
                {
                    if (i >= 5.00)
                    {
                        Console.WriteLine("{0:0.00}", i);
                    }
                }
                Console.WriteLine("\n");
                foreach (double i in prijzen)
                {
                    gemiddelde += i;
                }
                Console.WriteLine("Het gemidelde bedrag is {0}", gemiddelde / prijzen.Length);
            }
            AskForPrices();
        }
    }
}
