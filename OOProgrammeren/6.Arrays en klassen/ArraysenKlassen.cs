﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ArraysenKlassen
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("\nMaak een keuze: ");
            Console.WriteLine("Oefening 1: Prijzen met foreach (h11-prijzen)");
            Console.WriteLine("Oefening 2: Speelkaarten (h11-speelkaarten)");
            Console.WriteLine("Oefening 3: Organiseren van studenten (h11-organizer)");
            Console.WriteLine("Oefening 4: Hoger lager");
            int keuze = int.Parse(Console.ReadLine());

            switch (keuze)
            {
                case 1:
                    PrijzenMetForeach.Main();
                    break;
                case 2:
                    Speelkaart.ShowShuffledDeck(Speelkaart.GenerateDeck());
                    break;
                case 3:
                    Student.ExecuteStudentMenu();
                    break;
                case 4:
                    Speelkaart.HogerLager(Speelkaart.GenerateDeck());
                    break;
                default:
                    Console.WriteLine("Default");
                    break;
            }
        }
    }
}
