﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    enum Suites
    {
        Clubs,
        Hearts,
        Spades,
        Diamonds
    }
    class Speelkaart
    {

        private int getal;
        public int Getal
        {
            get
            {
                return getal;
            }
            set
            {
                if (getal > 0 && getal < 14)
                    getal = value;
            }
        }
        public Suites Kleur{ get; set; }


        public Speelkaart(int getal, Suites kleur)
        {
            this.getal = getal;
            this.Kleur = kleur;
        }

        static public List<Speelkaart> GenerateDeck()
        {
            List<Speelkaart> speelkaarten = new List<Speelkaart>();
            //for (int i = 1; i < 14; i++)
            //{
            //    Speelkaart spk = new Speelkaart(i, Suites.Clubs);
            //    speelkaarten.Add(spk);
            //}
            //for (int i = 1; i < 14; i++)
            //{
            //    Speelkaart spk = new Speelkaart(i, Suites.Diamonds);
            //    speelkaarten.Add(spk);
            //}
            //for (int i = 1; i < 14; i++)
            //{
            //    Speelkaart spk = new Speelkaart(i, Suites.Spades);
            //    speelkaarten.Add(spk);
            //}
            //for (int i = 1; i < 14; i++)
            //{
            //    Speelkaart spk = new Speelkaart(i, Suites.Hearts);
            //    speelkaarten.Add(spk);
            //}
            foreach (Suites suit in Enum.GetValues(typeof(Suites)))
            {
                for (int i = 1; i < 14; i++)
                {
                    Speelkaart spk = new Speelkaart(i, suit);
                    speelkaarten.Add(spk);
                }
            }
            return speelkaarten;
        }

        static public void ShowShuffledDeck(List<Speelkaart> kaarten)
        {
            for (int i = 52; i >= 1; i--)
            {
                Random rnd = new Random();
                int b = rnd.Next(0, i);
                Console.WriteLine(kaarten[b].getal + " " + kaarten[b].Kleur);
                kaarten.Remove(kaarten[b]);
            }
        }

        static public void HogerLager(List<Speelkaart> kaarten)
        {
            Console.WriteLine("Klaar om te spelen");
            int aantalKaarten = 52;
            int huidige;
            Random rnd = new Random();
            int vorige = rnd.Next(0, aantalKaarten);
            aantalKaarten--;
            Speelkaart vorigeKaart = kaarten[vorige];
            kaarten.Remove(vorigeKaart);
            while (aantalKaarten > 0)
            {
                huidige = rnd.Next(0, aantalKaarten);
                Speelkaart huidigeKaart = kaarten[huidige];
                aantalKaarten--;
                kaarten.Remove(huidigeKaart);
                Console.WriteLine("De vorige kaart had waarde " + vorigeKaart.Getal);
                Console.WriteLine("Hoger (0), Lager (1) of Gelijk (2)");
                int keuze = Convert.ToInt32(Console.ReadLine());
                switch (keuze)
                {
                    case 0:
                        if (!(huidigeKaart.Getal > vorigeKaart.Getal))
                        {
                            Console.WriteLine("U hebt verloren.");
                            aantalKaarten = 0;
                        }
                        else
                        {
                            vorigeKaart = huidigeKaart;
                        }
                        break;
                    case 1:
                        if (!(huidigeKaart.Getal < vorigeKaart.Getal))
                        {
                            Console.WriteLine("U hebt verloren.");
                            aantalKaarten = 0;
                        }
                        else
                        {
                            vorigeKaart = huidigeKaart;
                        }
                        break;
                    case 2:
                        if (!(huidigeKaart.Getal == vorigeKaart.Getal))
                        {
                            Console.WriteLine("U hebt verloren.");
                            aantalKaarten = 0;
                        }
                        else
                        {
                            vorigeKaart = huidigeKaart;
                        }
                        break;
                }
            }
            Console.WriteLine("Spel voorbij!");
        }
    }
}
