﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class MethodenAccessModifiersProperties
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("\nMaak een keuze: ");
            Console.WriteLine("Oefening 1: H8-Studentklasse");
            int keuze = int.Parse(Console.ReadLine());

            switch (keuze)
            {
                case 1:
                    Student.Main();
                    break;
                default:
                    Console.WriteLine("Default");
                    break;
            }
        }
    }
}

