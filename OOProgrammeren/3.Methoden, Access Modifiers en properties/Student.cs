﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    enum ClassGroups
    {
        EA1,
        EA2,
        EB1
    }
    class Student
    {
        private byte leeftijd;
        private byte markCommunicatie;
        private byte markProgrammeren;
        private byte markWebTech;

        public Student()
        {

        }

        public Student(string name, byte leeftijd, ClassGroups klas, byte markCommunicatie, byte markProgrammeren, byte markWebTech)
        {
            Name = name;
            this.leeftijd = leeftijd;
            Klas = klas;
            this.markCommunicatie = markCommunicatie;
            this.markProgrammeren = markProgrammeren;
            this.markWebTech = markWebTech;
        }

        public string Name { get; set; } = "Onbekend";
        public ClassGroups Klas { get; set; }
        public byte Leeftijd
        {
            get
            {
                return leeftijd;
            }
            set
            {
                if (value < 121)
                    leeftijd = value;
            }
        }

        public byte MarkCommunicatie
        {
            get
            {
                return markCommunicatie;
            }
            set
            {
                if (value < 21)
                    markCommunicatie = value;
            }
        }
        public byte MarkProgrammeren
        {
            get
            {
                return markProgrammeren;
            }
            set
            {
                if (value < 21)
                    markProgrammeren = value;
            }
        }
        public byte MarkWebtech
        {
            get
            {
                return markWebTech;
            }
            set
            {
                if (value < 21)
                    markWebTech = value;
            }
        }
        public double OverallMark
        {
            get
            {
                return (MarkCommunicatie + MarkProgrammeren + MarkWebtech) / 3.00;
            }
        }

        public virtual string ShowOverview()
        {
            return Name + ", " + Leeftijd + "\nKlas: " + Klas + "\n\nCijferrapport: \n" + "*****************\nCommunicatie: "
                + MarkCommunicatie + "\nProgrammeren: " + MarkProgrammeren + "\nWebTech: "
                + MarkWebtech + "\nGemiddelde: " + OverallMark.ToString("0.0") + "\n";
        }

        public static void ExecuteStudentMenu()
        {
            List<Student> studenten = new List<Student>();
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. Gegevens van de studenten tonen\n" +
                "2. Een nieuwe student toevoegen\n" +
                "3. Gegevens van een bepaalde student aanpassen\n" +
                "4. Een student uit het systeem verwijderen\n" +
                "5. Gemiddelde cijfer op communicatie tonen\n" +
                "6. Gemiddelde cijfer op programmeren tonen\n" +
                "7. Gemiddelde cijfer op webtechnologie tonen\n" +
                "8. Stoppen\n");
            int keuze = Convert.ToInt16(Console.ReadLine());
            while (keuze != 8 && keuze < 9)
            {
                switch (keuze)
                {
                    case 1:
                        foreach (Student stu in studenten)
                            Console.WriteLine(stu.ShowOverview());
                        break;
                    case 2:
                        Student stud = new Student();
                        studenten.Add(stud);
                        break;
                    case 3:
                        Console.WriteLine("Wat is de indexpositie van de student die je wilt aanpassen?");
                        int optie = Convert.ToInt16(Console.ReadLine());
                        Student gekozen = studenten[optie];
                        Console.WriteLine("Wat wil je aanpassen?");
                        Console.WriteLine("1. Naam\n" +
                            "2. Leeftijd\n" +
                            "3. Klasgroep\n" +
                            "4. Cijfer communicatie\n" +
                            "5. Cijfer programmeren\n" +
                            "6. Cijfer webtechnologie\n");
                        int optie2 = Convert.ToInt16(Console.ReadLine());
                        switch (optie2)
                        {
                            case 1:
                                Console.WriteLine("Wat is de nieuwe waarde?");
                                gekozen.Name = Console.ReadLine();
                                break;
                            case 2:
                                Console.WriteLine("Wat is de nieuwe waarde?");
                                gekozen.Leeftijd = Convert.ToByte(Console.ReadLine());
                                break;
                            case 3:
                                Console.WriteLine("Wat is de nieuwe waarde?");
                                string klas = Console.ReadLine();
                                if (klas == ClassGroups.EA1.ToString())
                                    gekozen.Klas = ClassGroups.EA1;
                                else if (klas == ClassGroups.EA2.ToString())
                                    gekozen.Klas = ClassGroups.EA2;
                                else
                                    gekozen.Klas = ClassGroups.EB1;
                                break;
                            case 4:
                                Console.WriteLine("Wat is de nieuwe waarde?");
                                gekozen.MarkCommunicatie = Convert.ToByte(Console.ReadLine());
                                break;
                            case 5:
                                Console.WriteLine("Wat is de nieuwe waarde?");
                                gekozen.MarkProgrammeren = Convert.ToByte(Console.ReadLine());
                                break;
                            case 6:
                                Console.WriteLine("Wat is de nieuwe waarde?");
                                gekozen.MarkWebtech = Convert.ToByte(Console.ReadLine());
                                break;
                        }
                        break;
                    case 4:
                        Console.WriteLine("Wat is de indexpositie van de te verwijderen student?");
                        int verwijderPos = Convert.ToInt16(Console.ReadLine());
                        studenten.Remove(studenten[verwijderPos]);
                        break;
                    case 5:
                        double aantal = 0.0;
                        int cijfer = 0;
                        foreach (Student stu in studenten)
                        {
                            cijfer += stu.MarkCommunicatie;
                            aantal++;
                        }
                        double gemiddelde = cijfer / aantal;
                        Console.WriteLine("Gemiddelde cijfer op communicatie: " + gemiddelde.ToString("0.0"));
                        break;
                    case 6:
                        aantal = 0;
                        cijfer = 0;
                        foreach (Student stu in studenten)
                        {
                            cijfer += stu.MarkProgrammeren;
                            aantal++;
                        }
                        gemiddelde = cijfer / aantal;
                        Console.WriteLine("Gemiddelde cijfer op communicatie: " + gemiddelde.ToString("0.0"));
                        break;
                    case 7:
                        aantal = 0;
                        cijfer = 0;
                        foreach (Student stu in studenten)
                        {
                            cijfer += stu.MarkWebtech;
                            aantal++;
                        }
                        gemiddelde = cijfer / aantal;
                        Console.WriteLine("Gemiddelde cijfer op communicatie: " + gemiddelde.ToString("0.0"));
                        break;
                }
                Console.WriteLine("Wat wil je doen?");
                keuze = Convert.ToInt16(Console.ReadLine());
            }
        }

        public static void Main()
        {
            Student stf = new Student();
            stf.Klas = ClassGroups.EA1;
            stf.Leeftijd = 21;
            stf.Name = "Johan Hulk";
            stf.MarkCommunicatie = 12;
            stf.MarkProgrammeren = 15;
            stf.MarkWebtech = 13;

            Student jkf = new Student();
            jkf.Klas = ClassGroups.EB1;
            jkf.Leeftijd = 19;
            jkf.Name = "Jan De Mulder";
            jkf.MarkCommunicatie = 19;
            jkf.MarkProgrammeren = 18;
            jkf.MarkWebtech = 17;

            Console.WriteLine(stf.ShowOverview() + "\n");
            Console.WriteLine(jkf.ShowOverview() + "\n");
        }
    }
}

