﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Task
    {
        public string description { get; set; }

        public Task(string description)
        {
            this.description = description;
            Console.WriteLine("Taak {0} is aangemaakt.", description);
        }
    }
}
