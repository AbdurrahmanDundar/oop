﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class WorkingStudent: Student
    {
        private byte workHours;

        public byte WorkHours
        {
            get { return workHours; }
            set
            {
                if (value > 20)
                {
                    workHours = 20;
                }
                else if (value < 1)
                {
                    workHours = 1;
                }
                else
                {
                    workHours = value;
                }

            }
        }

        public WorkingStudent(string name, byte leeftijd, ClassGroups klas, byte markCommunicatie, byte markProgrammeren, byte markWebTech): base (name, leeftijd, klas, markCommunicatie, markProgrammeren, markWebTech)
        {
            workHours = 10;
        }

        public WorkingStudent(string name, byte leeftijd, ClassGroups klas, byte markCommunicatie, byte markProgrammeren, byte markWebTech, byte workhours) : base(name, leeftijd, klas, markCommunicatie, markProgrammeren, markWebTech)
        {
            WorkHours = workhours;
        }



        public bool HasWorkToday()
        {
            Random rnd = new Random();
            if (rnd.Next(0, 2) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ShowOverview()
        {
            return base.ShowOverview() + "Statuut : werkstudent" + "\nAantal werkuren per week: " + WorkHours + "\n";
        }

        static public void DemonstrateWorkingStudent()
        {
            Student stu = new Student("Johan Hulk", 21, ClassGroups.EA1,12, 15, 13);

            WorkingStudent wstu = new WorkingStudent("Johannes Hulk", 19, ClassGroups.EA1, 10, 10, 10, 0);

            List<Student> studentenLijst = new List<Student>();
            studentenLijst.Add(stu);
            studentenLijst.Add(wstu);
            foreach (Student s in studentenLijst)
            {
                Console.WriteLine(s.ShowOverview());
            }
        }
    }
}
