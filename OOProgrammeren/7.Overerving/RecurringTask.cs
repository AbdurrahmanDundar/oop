﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class RecurringTask: Task
    {
        public byte dagen { get; set; }
        public RecurringTask(string beschrijving, byte dagen): base(beschrijving)
        {
            this.dagen = dagen;
            beschrijving = this.description;
            Console.WriteLine("Deze taak moet om de {0} dagen herhaald worden", dagen);
        }

        static public void DemonstrateTasks() 
        {
            ArrayList taken = new ArrayList();
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. Een taak maken.");
            Console.WriteLine("2. Een terugkerende taak maken.");
            Console.WriteLine("3. Stoppen");
            int keuze = Convert.ToInt32(Console.ReadLine());
            while (keuze == 1 || keuze == 2)
            {
                if(keuze == 1)
                {
                    Console.WriteLine("Beschrijving van de taak?");
                    Task tk = new Task(Console.ReadLine());
                    taken.Add(tk);
                }
                else{
                    Console.WriteLine("Bescrijving van de taak?");
                    string beschr = Console.ReadLine();
                    Console.WriteLine("Aantal dagen tussen herhaling?");
                    byte dagen = Convert.ToByte(Console.ReadLine());
                    RecurringTask rtk = new RecurringTask(beschr, dagen);
                    taken.Add(rtk);
                }

                Console.WriteLine("Wat wil je doen?");
                Console.WriteLine("1. Een taak maken.");
                Console.WriteLine("2. Een terugkerende taak maken.");
                Console.WriteLine("3. Stoppen");
                keuze = Convert.ToInt32(Console.ReadLine());
            }
        }
    }
}
