﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Overerving
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("\nMaak een keuze: ");
            Console.WriteLine("Oefening 1: H12 een bestaande klasse uitbreiden via overerving.");
            Console.WriteLine("Oefening 2: H12 Klassen met aangepaste constructor maken.");
            Console.WriteLine("Oefening 3: H12 Ziekenhuis.");
            int keuze = int.Parse(Console.ReadLine());

            switch (keuze)
            {
                case 1:
                    WorkingStudent.DemonstrateWorkingStudent();
                    break;
                case 2:
                    RecurringTask.DemonstrateTasks();
                    break;
                case 3:
                    InsuredPatient.DemonstratePatients();
                    break;
                default:
                    Console.WriteLine("Default");
                    break;
            }
        }
    }
}
