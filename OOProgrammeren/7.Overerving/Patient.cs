﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Patient
    {
        public string naam{ get; set; }
        public uint verblijfsduur { get; set; }

        public Patient(string naam, uint verblijfsduur)
        {
            this.naam = naam;
            this.verblijfsduur = verblijfsduur;
        }

        public virtual void showCost()
        {
            uint kost = 50 + (20 * verblijfsduur);
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("{0}, een gewone patient die {1} uur in het ziekenhuis gelegen heeft, betaalt \u20AC{2}.", naam, verblijfsduur, kost);
        }
    }
}
