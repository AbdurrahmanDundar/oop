﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class InsuredPatient: Patient
    {
        public InsuredPatient(string naam, uint verblijfsduur): base(naam, verblijfsduur)
        {
        }

        public override void showCost()
        {
            uint kost = 50 + (20 * verblijfsduur);
            uint iKost = Convert.ToUInt16(kost -(kost * 0.1));
            Console.WriteLine("{0}, een gewone patient die {1} uur in het ziekenhuis gelegen heeft, betaalt \u20AC{2}.", naam, verblijfsduur, iKost);
        }
        static public void DemonstratePatients()
        {
            Patient pt = new Patient("Vincent", 12);
            InsuredPatient ipt = new InsuredPatient("Tim", 12);
            pt.showCost();
            ipt.showCost();
        }
    }
}
