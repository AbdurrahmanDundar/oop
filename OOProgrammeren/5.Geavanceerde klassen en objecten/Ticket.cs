﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Ticket
    {
        static Random rnd = new Random();
        public byte Prize { get; set; }

        public Ticket()
        {
            Prize = Convert.ToByte(rnd.Next(1, 101));
        }

        public static void Raffle()
        {
            for (int i = 0; i<10; i++)
            {
                Ticket a = new Ticket();
                Console.Write("Waarde van het lotje {0} \n", a.Prize);
            }
        }
    }
}
