﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class GeavanceerdeKlassenEnObjecten
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("\nMaak een keuze: ");
            Console.WriteLine("Pokémons makkelijk aanmaken (h10-pokeconstructie)");
            Console.WriteLine("Pokémons nog makkelijker aanmaken (h10-chaining)");
            Console.WriteLine("Oefening 3: Globale statistieken bijhouden (h10-pokebattlecount)");
            Console.WriteLine("Oefening 4: Gemeenschappelijke kenmerken (h10-tombola)");
            Console.WriteLine("Oefening 5: CSV");
            int keuze = int.Parse(Console.ReadLine());

            switch (keuze)
            {
                case 1:
                    Pokemon.MakePokemon();
                    break;
                case 2:
                    Pokemon.ConstructPokemonChained();
                    break;
                case 3:
                    Pokemon.DemonstrateCounter();
                    break;
                case 4:
                    Ticket.Raffle();
                    break;
                case 5:
                    CSVDemo.Run();
                    break;
                default:
                    Console.WriteLine("Default");
                    break;
            }
        }
    }
}
