﻿using System;

namespace OOP
{
    public class Program
    {
        static void Main(string[] args)
        {
            //git status
            //git add <file>??????
            //git commit -m "Description"
            //git push -f origin master
            Console.WriteLine("Kies een onderwep: ");
            Console.WriteLine("3. Methoden, Access Modiefier en properties");
            Console.WriteLine("4. GeheugenmanagementBijKlassen");
            Console.WriteLine("5. Geavanceerde klasses en objecten");
            Console.WriteLine("6. Arrays en klassen");
            Console.WriteLine("7. Overerving");
            Console.WriteLine("");

            int keuze = int.Parse(Console.ReadLine());
            switch (keuze)
            {

                case 3:
                    MethodenAccessModifiersProperties.StartSubmenu();
                    break;
                case 4:
                    GeheugenmanagementBijKlassen.StartSubmenu();
                    break;
                case 5:
                    GeavanceerdeKlassenEnObjecten.StartSubmenu();
                    break;
                case 6:
                    ArraysenKlassen.StartSubmenu();
                    break;
                case 7:
                    Overerving.StartSubmenu();
                    break;
                default:
                    Console.WriteLine("Default");
                    break;
            }
        }
    }
}
