﻿using System;
using System.Collections.Generic;
using System.Text;
using CColor = System.Drawing.Color;

namespace OOP
{
    class Pokemon
    {
        private int maxHP;
        private int hp;

        public int MaxHP
        {
            get
            {
                return maxHP;
            }
            set
            {
                if (value > 20 && value < 1000)
                    maxHP = value;
                else if (value <= 20)
                    maxHP = 20;
                else
                    maxHP = 1000;
            }
        }
        public int HP
        {
            get
            {
                return hp;
            }
            set
            {
                if (value >= 0)
                {
                    if (value <= MaxHP)
                    {
                        hp = value;
                    }
                    else
                    {
                        hp = MaxHP;
                    }
                }
                else
                {
                    hp = 0;
                }
            }
        }
        public PokeSpecies PokeSpecies { get; set; }
        public PokeType PokeType { get; set; }

        public Pokemon(int maxHP, int hp, PokeSpecies pokeSpecies, PokeType pokeType)
        {
            MaxHP = maxHP;
            HP = hp;
            PokeSpecies = pokeSpecies;
            PokeType = pokeType;
        }

        public Pokemon(int maxHP, PokeSpecies pokeSpecies, PokeType pokeType) : this(maxHP, maxHP/2, pokeSpecies, pokeType)
        {
            HP = maxHP / 2;
        }
        public Pokemon()
        {
        }


        public static void MakePokemon()
        {
            Pokemon bulbasaur = new Pokemon(20, 0, PokeSpecies.Bulbasaur, PokeType.Grass);
            bulbasaur.Attack();

            Pokemon charmander = new Pokemon(20, 0, PokeSpecies.Charmander, PokeType.Fire);
            charmander.Attack();

            Pokemon squirtle = new Pokemon(20, 0, PokeSpecies.Squirtle, PokeType.Water);
            squirtle.Attack();

            Pokemon pikachu = new Pokemon(20, 0, PokeSpecies.Pikachu, PokeType.Electric);
            pikachu.Attack();

        }

        public void Attack()
        {
            if(this.PokeType == PokeType.Grass)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(PokeSpecies.ToString().ToUpper() + "!\n");
            }
            else if(this.PokeType == PokeType.Fire)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(PokeSpecies.ToString().ToUpper() + "!\n");
            }
            else if (this.PokeType == PokeType.Water)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(PokeSpecies.ToString().ToUpper() + "!\n");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(PokeSpecies.ToString().ToUpper() + "!\n");
            }
        }
        public static Pokemon FirstConsciousPokemon(Pokemon[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].HP >= 1)
                {
                    return arr[i];
                }
            }
            return null;
        }

        public static void TestConsciousPokemon()
        {
            Pokemon bulbasaur = new Pokemon(1000, 0, PokeSpecies.Bulbasaur, PokeType.Grass);
            Pokemon charmander = new Pokemon(1000, 0, PokeSpecies.Charmander, PokeType.Fire);
            Pokemon squirtle = new Pokemon(1000, 2, PokeSpecies.Squirtle, PokeType.Water);
            Pokemon pikachu = new Pokemon(1000, 0, PokeSpecies.Pikachu, PokeType.Electric);

            Pokemon[] array = { bulbasaur, charmander, squirtle, pikachu };

            Pokemon first = FirstConsciousPokemon(array);
            first.Attack();
        }

        public static void TestConsciousPokemonSafe()
        {
            Pokemon bulbasaur = new Pokemon(1000, 0, PokeSpecies.Bulbasaur, PokeType.Grass);
            Pokemon charmander = new Pokemon(1000, 0, PokeSpecies.Charmander, PokeType.Fire);
            Pokemon squirtle = new Pokemon(1000, 0, PokeSpecies.Squirtle, PokeType.Water);
            Pokemon pikachu = new Pokemon(1000, 0, PokeSpecies.Pikachu, PokeType.Electric);

            Pokemon[] array = { bulbasaur, charmander, squirtle, pikachu };

            Pokemon first = FirstConsciousPokemon(array);
            if(first != null)
                first.Attack();
            else
                Console.WriteLine("Al je Pokémon zijn KO! Haast je naar het Pokémon center!");
        }
        public static void RestoreHP(Pokemon pokemon)
        {
            pokemon.HP = pokemon.MaxHP;
        }
        public static void DemoRestoreHP()
        {
            // aanmaken van array bewusteloze Pokemon van 4 soorten zoals eerder: zelf doen
            Pokemon bulbasaur = new Pokemon(25, 0, PokeSpecies.Bulbasaur, PokeType.Grass);
            Pokemon charmander = new Pokemon(25, 0, PokeSpecies.Charmander, PokeType.Fire);
            Pokemon squirtle = new Pokemon(25, 0, PokeSpecies.Squirtle, PokeType.Water);
            Pokemon pikachu = new Pokemon(25, 0, PokeSpecies.Pikachu, PokeType.Electric);

            Pokemon[] pokemon = { bulbasaur, charmander, squirtle, pikachu };

            for (int i = 0; i < pokemon.Length; i++)
            {
                Pokemon.RestoreHP(pokemon[i]);
            }
            for (int i = 0; i < pokemon.Length; i++)
            {
                Console.WriteLine(pokemon[i].HP);
            }
        }
        public static void FightOutcome(Pokemon pk1, Pokemon pk2, Random rnd)
        {
            if (pk1.HP == 0 || pk2.HP == 0 || pk1 == null || pk2 == null)
            {
                if (pk1.HP == 0 && pk2.HP != 0)
                {
                    Console.WriteLine(Outcome.Loss.ToString());
                }
                else if (pk1.HP != 0 && pk2.HP == 0)
                {
                    Console.WriteLine(Outcome.Win.ToString());
                }
                else
                    Console.WriteLine(Outcome.Undecided.ToString());
            }
            else
            {
                if (rnd.Next(1, 3) == 1)
                {
                    while (pk1.HP != 0 && pk2.HP != 0)
                    {
                        pk1.Attack();
                        pk2.HP -= rnd.Next(0, 21);
                        pk2.Attack();
                        pk1.HP -= rnd.Next(0, 21);
                    }
                    if (pk2.HP == 0)
                        Console.WriteLine(Outcome.Win.ToString());
                    else
                        Console.WriteLine(Outcome.Loss.ToString());
                }
                else
                {
                    while (pk1.HP != 0 && pk2.HP != 0)
                    {
                        pk2.Attack();
                        pk1.HP -= rnd.Next(0, 21);
                        pk1.Attack();
                        pk2.HP -= rnd.Next(0, 21);
                    }
                    if (pk2.HP == 0)
                        Console.WriteLine(Outcome.Win.ToString());
                    else
                        Console.WriteLine(Outcome.Loss.ToString());
                }
            }          
        }

        public static void DemoFightOutcome()
        {
            Pokemon bulbasaur = new Pokemon(20, 0, PokeSpecies.Bulbasaur, PokeType.Grass);
            Pokemon charmander = new Pokemon(20, 20, PokeSpecies.Charmander, PokeType.Fire);
            Random rnd =  new Random();
            Pokemon.FightOutcome(bulbasaur, charmander, rnd);
        }

        public static void ConstructPokemonChained()
        {
            Pokemon pika = new Pokemon(40, PokeSpecies.Pikachu, PokeType.Electric);
            Console.WriteLine("De nieuwe Squirtle heeft maximum {0} HP en heeft momenteel {1} HP.", pika.MaxHP, pika.hp);
        }

        public static void DemonstrateCounter()
        {
            Random rnd = new Random();
            int attacksWater = 0;
            int attacksElectric = 0;
            int attacksFire = 0;
            int attacksGrass = 0;
            for (int i = 0; i<5; i++)
            {
                if (rnd.Next(0, 5) == 1)
                {
                    Pokemon poke = new Pokemon(20, 20, PokeSpecies.Bulbasaur, PokeType.Grass);
                    for (int a = 1; a <= rnd.Next(5,11); a++)
                    {
                        poke.Attack();
                        attacksGrass++;
                    }
                }
                else if (rnd.Next(0, 5) == 1)
                {
                    Pokemon poke = new Pokemon(20, 20, PokeSpecies.Charmander, PokeType.Fire);
                    for (int a = 1; a <= rnd.Next(5, 11); a++)
                    {
                        poke.Attack();
                        attacksFire++;
                    }
                }
                else if (rnd.Next(0, 5) == 1)
                {
                    Pokemon poke = new Pokemon(20, 20, PokeSpecies.Squirtle, PokeType.Water);
                    for (int a = 1; a <= rnd.Next(5, 11); a++)
                    {
                        poke.Attack();
                        attacksWater++;
                    }
                }
                else 
                {
                    Pokemon poke = new Pokemon(20, 20, PokeSpecies.Pikachu, PokeType.Electric);
                    for (int a = 1; a <= rnd.Next(5, 11); a++)
                    {
                        poke.Attack();
                        attacksElectric++;
                    }
                }
            }
            Console.WriteLine("Aantal aanvallen van Pokémon met type `Grass`: {0}", attacksGrass);
            Console.WriteLine("Aantal aanvallen van Pokémon met type `Fire`: {0}", attacksFire);
            Console.WriteLine("Aantal aanvallen van Pokémon met type `Electric`: {0}", attacksElectric);
            Console.WriteLine("Aantal aanvallen van Pokémon met type `Water`: {0}", attacksWater);
        }
    }
}
