﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class GeheugenmanagementBijKlassen
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("\nMaak een keuze: ");
            Console.WriteLine("Oefening 1: klasse Pokemon (h9-pokeattack)");
            Console.WriteLine("Oefening 2 (h9-consciouspokemon)");
            Console.WriteLine("Oefening 3 (h9-consciouspokemon-improved)");
            Console.WriteLine("Oefening 4: value en reference (h9-pokevalueref)");
            Console.WriteLine("Oefening 5: uitkomst gevecht (h9-fight)");
            int keuze = int.Parse(Console.ReadLine());

            switch (keuze)
            {
                case 1:
                    Pokemon.MakePokemon();
                    break;
                case 2:
                    Pokemon.TestConsciousPokemon();
                    break;
                case 3:
                    Pokemon.TestConsciousPokemonSafe();
                    break;
                case 4:
                    Pokemon.DemoRestoreHP();
                    break;
                case 5:
                    Pokemon.DemoFightOutcome();
                    break;
                default:
                    Console.WriteLine("");
                    break;
            }
        }
    }
}
